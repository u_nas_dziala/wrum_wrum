import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:wrum_wrum/model/passanger_info.dart';
import 'package:wrum_wrum/model/route_tras.dart';

class LocalDBProvider {
  LocalDBProvider._();

  static final LocalDBProvider db = LocalDBProvider._();

  Database _database;

  Future<Database> getDatabase() async {
    if(_database !=  null) {
      return _database;
    }

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "localDB.db");
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {
        debugPrint("make create sql great again");
      },
      onCreate: (db, version) async {
        try {
          await db.execute('''
        CREATE TABLE passengers (
          id INTEGER PRIMARY KEY,
          name TEXT,
          surname TEXT,
          phone TEXT,
          addressFrom TEXT,
          addressTo TEXT,
          baggage TEXT,
          formattedDate TEXT,
          formattedTime TEXT,
          route INTEGER
        );
        ''');
        await db.execute('''
        CREATE TABLE routes (
          id INTEGER PRIMARY KEY,
          addressFrom TEXT,
          addressTo TEXT,
          formattedDate TEXT,
          formattedTime TEXT,
          maxPeople INTEGER,
          price INTEGER,
          baggage TEXT,
          ended INTEGER
        );
        ''');
        }
        catch(e) {
          print(e);
        }
      }


    );
  }
  newRoute(RouteTras newRoute) async {
    final db = await getDatabase();

    var res = await db.rawInsert('''
      INSERT INTO routes (
        addressFrom,
        addressTo,
        formattedDate,
        formattedTime,
        maxPeople,
        price,
        baggage,
        ended
      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?);
    
    ''', [
          newRoute.addressFrom,
          newRoute.addressTo,
          newRoute.formattedDate,
          newRoute.formattedTime,
          newRoute.maxPeople,
          newRoute.price,
          newRoute.baggage,
          newRoute.ended ? 0 : 1
        ]);
    return res;
  }

  newPassenger(PassengerInfo newPassenger, int routeId) async {
    final db = await getDatabase();
    try {
      var res = await db.rawInsert('''
      INSERT INTO passengers (
        name,
        surname,
        phone,
        addressFrom,
        addressTo,
        baggage,
        formattedDate,
        formattedTime,
        route
      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);
    
    ''', [
        newPassenger.name,
        newPassenger.surname,
        newPassenger.phone,
        newPassenger.addressFrom,
        newPassenger.addressTo,
        newPassenger.baggage,
        newPassenger.formattedDate,
        newPassenger.formattedTime,
        routeId,
      ]);

      return res;
    } catch(e) {
      print(e);
    }
  }

  updateIfPlannedIsHistorical() async {
    final db = await getDatabase();

    DateTime today = DateTime.now();

    List<RouteTras> allRoutes = await getRoutes(false);

    allRoutes.forEach((route) async {
      DateTime routeDate = new DateFormat("dd/MM/yyyy").parse(route.formattedDate);
      print(routeDate);


      if(routeDate.isAfter(today)) {
        //query to update
        await db.rawUpdate(
          '''
        UPDATE routes
        SET ended = 0
        WHERE id = ?;
        ''', [route.id]
        );
      }
    });
  }

  Future<List<dynamic>> getRoutes(bool finished) async {
    try {
    print("info");
    final db = await getDatabase();
    var res;
    try {
      res = await db.query("routes",
          where: "ended = ?",
          whereArgs: [finished ? 0 : 1]
      );
    } catch (e) {
      print(e);
    }

    print("info2");

    if(res.length == 0) {
      print("test4");

      return null;
    } else {
      print("test3");

      List<RouteTras> routes = res.map<RouteTras>((element) {
        print(element);
        return RouteTras.fromJSON(element);
      }).toList() as List<RouteTras>;
      print("test1");


      for (int i = 0; i < routes.length; i++) {
        var passengers;
        try {
          passengers = await db.query("passengers",
              where: " route = ?",
              whereArgs: [routes[i].id]
          );
        } catch (e) {
          print(e);
        }

        for (int j = 0; j < passengers.length; j++) {
          routes[i].passengerList.add(PassengerInfo.fromJSON(passengers[j]));
        }
      }
      print("test");

      return routes;
    }
    } catch(e) {
      print(e);
    }

  }

  @override
  int get hashCode {
 }

  Future<dynamic> getPassengerInfo() async {
    final db = await getDatabase();
    var res = await db.query("passenger");
    if(res.length == 0) {
      return null;
    } else {
      PassengerInfo passengerInfo = PassengerInfo.fromJSON(res[0]);
      return passengerInfo;
    }
  }
}