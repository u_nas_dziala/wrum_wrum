import 'dart:async';
import 'dart:core';

import 'package:wrum_wrum/db/local_db_provider.dart';
import 'package:wrum_wrum/model/route_tras.dart';


class RouteRepository {

  Future<List<RouteTras>> loadArchivedRoutes() async {
    return await LocalDBProvider.db.getRoutes(true);
  }

  Future<List<RouteTras>> loadCurrentRoutes() async {
    return await LocalDBProvider.db.getRoutes(false);
  }

}