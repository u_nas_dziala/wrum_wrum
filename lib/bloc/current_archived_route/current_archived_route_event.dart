part of 'current_archived_route_bloc.dart';

abstract class CurrentArchivedRouteEvent extends Equatable {
  const CurrentArchivedRouteEvent();

  @override
  List<Object> get props => [];
}

class CurrentArchivedRouteEnded extends CurrentArchivedRouteEvent {}

class CurrentArchivedRouteStarted extends CurrentArchivedRouteEvent {
  final RouteTras route;

  const CurrentArchivedRouteStarted(this.route);

  @override
  List<Object> get props => [route];
}