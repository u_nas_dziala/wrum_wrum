import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wrum_wrum/model/route_tras.dart';

part 'current_archived_route_event.dart';
part 'current_archived_route_state.dart';

class CurrentArchivedRouteBloc extends Bloc<CurrentArchivedRouteEvent, CurrentArchivedRouteState> {
  CurrentArchivedRouteBloc() : super(CurrentArchivedRouteNotSelected());

  @override
  Stream<CurrentArchivedRouteState> mapEventToState(
      CurrentArchivedRouteEvent event,
      ) async* {
    if(event is CurrentArchivedRouteStarted) {
      yield* _mapCurrentArchivedRouteStarted(event);
    } else if(event is CurrentArchivedRouteEnded) {
      yield* _mapCurrentArchivedRouteEnded(event);
    }
  }

  Stream<CurrentArchivedRouteState> _mapCurrentArchivedRouteStarted(CurrentArchivedRouteStarted event) async* {
    yield CurrentArchivedRouteSelected(event.route);
  }

  Stream<CurrentArchivedRouteState> _mapCurrentArchivedRouteEnded(CurrentArchivedRouteEnded event) async* {
    yield CurrentArchivedRouteNotSelected();
  }
}
