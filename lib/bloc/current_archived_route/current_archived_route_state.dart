part of 'current_archived_route_bloc.dart';

abstract class CurrentArchivedRouteState extends Equatable {
  const CurrentArchivedRouteState();

  @override
  List<Object> get props => [];
}

class CurrentArchivedRouteNotSelected extends CurrentArchivedRouteState {}

class CurrentArchivedRouteSelected extends CurrentArchivedRouteState {
  final RouteTras route;

  CurrentArchivedRouteSelected(this.route);

  @override
  List<Object> get props => [route];
}
