part of 'current_route_bloc.dart';

abstract class CurrentRouteState extends Equatable {
  const CurrentRouteState();

  @override
  List<Object> get props => [];
}

class CurrentRouteNotSelected extends CurrentRouteState {}

class CurrentRouteSelected extends CurrentRouteState {
  final RouteTras route;
  final int magic;

  CurrentRouteSelected(this.route, this.magic);

  @override
  List<Object> get props => [route, magic];
}
