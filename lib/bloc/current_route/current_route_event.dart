part of 'current_route_bloc.dart';

abstract class CurrentRouteEvent extends Equatable {
  const CurrentRouteEvent();

  @override
  List<Object> get props => [];
}

class CurrentRouteEnded extends CurrentRouteEvent {}

class CurrentRoutePassengerAdded extends CurrentRouteEvent {
  final PassengerInfo passengerInfo;
  final int routeID;

  const CurrentRoutePassengerAdded(this.passengerInfo, this.routeID);

  @override
  List<Object> get props => [passengerInfo];
}

class CurrentRouteStarted extends CurrentRouteEvent {
  final RouteTras route;

  const CurrentRouteStarted(this.route);

  @override
  List<Object> get props => [route];
}
