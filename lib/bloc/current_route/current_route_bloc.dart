import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wrum_wrum/db/local_db_provider.dart';
import 'package:wrum_wrum/model/passanger_info.dart';
import 'package:wrum_wrum/model/route_tras.dart';

part 'current_route_event.dart';
part 'current_route_state.dart';

class CurrentRouteBloc extends Bloc<CurrentRouteEvent, CurrentRouteState> {
  CurrentRouteBloc() : super(CurrentRouteNotSelected());

  @override
  Stream<CurrentRouteState> mapEventToState(
    CurrentRouteEvent event,
  ) async* {
    if(event is CurrentRouteStarted) {
      yield* _mapCurrentRouteStarted(event);
    } else if(event is CurrentRouteEnded) {
      yield* _mapCurrentRouteEnded(event);
    } else if(event is CurrentRoutePassengerAdded) {
      yield* _mapCurrentRoutePassengerAdded(event);
    }
  }

  Stream<CurrentRouteState> _mapCurrentRouteStarted(CurrentRouteStarted event) async* {
    yield CurrentRouteSelected(event.route, 0);
  }

  Stream<CurrentRouteState> _mapCurrentRouteEnded(CurrentRouteEnded event) async* {
    yield CurrentRouteNotSelected();
  }

  Stream<CurrentRouteState> _mapCurrentRoutePassengerAdded(CurrentRoutePassengerAdded event) async* {
    RouteTras route = (state as CurrentRouteSelected).route;
    RouteTras updatedRoute = route.copyWith(
      passengers: [...route.passengerList, event.passengerInfo]
    );

    await LocalDBProvider.db.newPassenger(event.passengerInfo, event.routeID);

    yield CurrentRouteSelected(updatedRoute, (state as CurrentRouteSelected).magic+1);
  }
}
