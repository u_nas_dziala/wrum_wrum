part of 'archived_routes_bloc.dart';

abstract class ArchivedRoutesState extends Equatable {
  const ArchivedRoutesState();

  @override
  List<Object> get props => [];
}

class ArchivedRoutesLoadInProgress extends ArchivedRoutesState {}

class ArchivedRoutesLoadSuccess extends ArchivedRoutesState {
  final List<RouteTras> routes;

  ArchivedRoutesLoadSuccess([this.routes = const []]);

  @override
  List<Object> get props => [routes];

}

class ArchivedRoutesLoadFailure extends ArchivedRoutesState {}
