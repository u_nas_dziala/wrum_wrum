part of 'archived_routes_bloc.dart';

abstract class ArchivedRoutesEvent extends Equatable {
  const ArchivedRoutesEvent();

  @override
  List<Object> get props => [];
}

class ArchivedRoutesLoaded extends ArchivedRoutesEvent {}

class ArchivedRouteAdded extends ArchivedRoutesEvent {
  final RouteTras routeTras;

  const ArchivedRouteAdded(this.routeTras);

  @override
  List<Object> get props => [routeTras];
}

class ArchivedRouteDeleted extends ArchivedRoutesEvent {
  final RouteTras routeTras;

  const ArchivedRouteDeleted(this.routeTras);

  @override
  List<Object> get props => [routeTras];
}
