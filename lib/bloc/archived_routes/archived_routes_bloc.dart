import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wrum_wrum/model/route_tras.dart';
import 'package:wrum_wrum/repository/route_repository.dart';

part 'archived_routes_event.dart';
part 'archived_routes_state.dart';

class ArchivedRoutesBloc extends Bloc<ArchivedRoutesEvent, ArchivedRoutesState> {
  final RouteRepository routeRepository;

  ArchivedRoutesBloc(this.routeRepository) : super(ArchivedRoutesLoadInProgress());

  @override
  Stream<ArchivedRoutesState> mapEventToState(ArchivedRoutesEvent event,) async* {
    if(event is ArchivedRoutesLoaded) {
      yield* _mapRouteLoadedToState();
    } else if(event is ArchivedRouteAdded) {
      yield* _mapRouteAddedToState(event);
    }
  }

  Stream<ArchivedRoutesState> _mapRouteLoadedToState() async* {
    try {
      final route = await this.routeRepository.loadArchivedRoutes();
      yield ArchivedRoutesLoadSuccess(
        route
      );
    } catch(_) {
      yield ArchivedRoutesLoadFailure();
    }
  }

  Stream<ArchivedRoutesState> _mapRouteAddedToState(ArchivedRouteAdded event) async* {
    if(state is ArchivedRoutesLoadSuccess) {
      final List<RouteTras> updatedRoutes = (state as ArchivedRoutesLoadSuccess).routes
          ..add(event.routeTras);
      yield ArchivedRoutesLoadSuccess(updatedRoutes);
    }
  }

  Stream<ArchivedRoutesState> _mapRouteDeletedFromState(ArchivedRouteDeleted event) async* {
    if(state is ArchivedRoutesLoadSuccess) {
      final List<RouteTras> updatedRoutes = (state as ArchivedRoutesLoadSuccess)
          .routes
          .where((route) => route.id != event.routeTras.id)
          .toList();
      yield ArchivedRoutesLoadSuccess(updatedRoutes);
    }
  }
}
