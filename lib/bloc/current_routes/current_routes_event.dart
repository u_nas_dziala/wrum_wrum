part of 'current_routes_bloc.dart';

abstract class CurrentRoutesEvent extends Equatable {
  const CurrentRoutesEvent();

  @override
  List<Object> get props => [];
}

class CurrentRoutesLoaded extends CurrentRoutesEvent {}

class CurrentRouteAdded extends CurrentRoutesEvent {
  final RouteTras routeTras;

  const CurrentRouteAdded(this.routeTras);

  @override
  List<Object> get props => [routeTras];
}

class CurrentRouteDeleted extends CurrentRoutesEvent {
  final RouteTras routeTras;

  const CurrentRouteDeleted(this.routeTras);

  @override
  List<Object> get props => [routeTras];
}
