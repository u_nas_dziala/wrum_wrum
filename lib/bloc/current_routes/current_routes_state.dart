part of 'current_routes_bloc.dart';

abstract class CurrentRoutesState extends Equatable {
  const CurrentRoutesState();

  @override
  List<Object> get props => [];
}

class CurrentRoutesLoadInProgress extends CurrentRoutesState {}

class CurrentRoutesLoadSuccess extends CurrentRoutesState {
  final List<RouteTras> routes;

  CurrentRoutesLoadSuccess([this.routes = const []]);

  @override
  List<Object> get props => [routes];

}

class CurrentRoutesLoadFailure extends CurrentRoutesState {}
