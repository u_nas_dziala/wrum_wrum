import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wrum_wrum/model/route_tras.dart';
import 'package:wrum_wrum/repository/route_repository.dart';

part 'current_routes_event.dart';
part 'current_routes_state.dart';

class CurrentRoutesBloc extends Bloc<CurrentRoutesEvent, CurrentRoutesState> {
  final RouteRepository routeRepository;

  CurrentRoutesBloc(this.routeRepository) : super(CurrentRoutesLoadInProgress());

  @override
  Stream<CurrentRoutesState> mapEventToState(CurrentRoutesEvent event,) async* {
    if(event is CurrentRoutesLoaded) {
      yield* _mapRouteLoadedToState();
    } else if(event is CurrentRouteAdded) {
      yield* _mapRouteAddedToState(event);
    }
  }

  Stream<CurrentRoutesState> _mapRouteLoadedToState() async* {
    try {
      final route = await this.routeRepository.loadCurrentRoutes();
      yield CurrentRoutesLoadSuccess(
          route
      );
    } catch(_) {
      yield CurrentRoutesLoadFailure();
    }
  }

  Stream<CurrentRoutesState> _mapRouteAddedToState(CurrentRouteAdded event) async* {
    if(state is CurrentRoutesLoadSuccess) {
      final List<RouteTras> updatedRoutes = (state as CurrentRoutesLoadSuccess).routes
        ..add(event.routeTras);
      yield CurrentRoutesLoadSuccess(updatedRoutes);
    }
  }

  Stream<CurrentRoutesState> _mapRouteDeletedFromState(CurrentRouteDeleted event) async* {
    if(state is CurrentRoutesLoadSuccess) {
      final List<RouteTras> updatedRoutes = (state as CurrentRoutesLoadSuccess)
          .routes
          .where((route) => route.id != event.routeTras.id)
          .toList();
      yield CurrentRoutesLoadSuccess(updatedRoutes);
    }
  }
}
