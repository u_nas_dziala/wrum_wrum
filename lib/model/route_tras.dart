import 'dart:collection';
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:wrum_wrum/model/passanger_info.dart';

// enum LuggageType {
//   Backpack,
//   SUITCASE_S,
//   SUITCASE_M,
//   SUITCASE_X
// }

class RouteTras extends Equatable {
  final int id;
  final String addressFrom;
  final String addressTo;
  final String formattedDate;
  final String formattedTime;
  final int maxPeople;
  final int price;
  final String baggage;
  final bool ended;
  final List<PassengerInfo> passengerList;

  RouteTras(
  {
    this.id,
      this.addressFrom,
      this.addressTo,
      this.formattedDate,
      this.formattedTime,
      this.maxPeople,
      this.price,
      this.baggage,
      this.ended,
      this.passengerList
  }
    );

  RouteTras copyWith({List<PassengerInfo> passengers}) => RouteTras(
    id: id,
    addressFrom: addressFrom,
    addressTo: addressTo,
    formattedDate: formattedDate,
    formattedTime: formattedTime,
    maxPeople: maxPeople,
    price: price,
    baggage: baggage,
    ended: ended,
    passengerList: passengers ?? passengerList
  );

  RouteTras.fromJSON(json)  :
        id = json["id"],
        addressFrom = json["addressFrom"],
        addressTo = json["addressTo"],
        formattedDate = json["formattedDate"],
        formattedTime = json["formattedTime"],
        maxPeople = json["maxPeople"],
        price = json["price"],
        baggage = json["baggage"],
        ended = json["ended"] == "0" ? true : false,
        passengerList = [];

  // static HashMap<LuggageType, int> _loadLuggageLimit(json) {
  //   print(json["luggageLimit"]);
  //   return json["luggageLimit"];
  // }

  String toJSON() {
    final routeObj = {
      "id" : id,
      "addressFrom" : addressFrom,
      "addressTo" : addressTo,
      "formattedDate" : formattedDate,
      "formattedTime" : formattedTime,
      "maxPeople" : maxPeople,
      "price" : price,
      "baggage" : baggage,
      "ended" : ended,
      "passengerList" : passengerList
      // "luggageLimit": json.encode(luggageLimit),
    };

    return JsonEncoder().convert(routeObj);
  }

  @override
  List<Object> get props =>[
    id,
    addressFrom,
    addressTo,
    formattedDate,
    formattedTime,
    maxPeople,
    price,
    baggage,
    ended,
  ];
}