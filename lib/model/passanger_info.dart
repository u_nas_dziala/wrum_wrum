import 'dart:convert';

import 'package:equatable/equatable.dart';

class PassengerInfo extends Equatable {
  final int id;
  final String name;
  final String surname;
  final String phone;
  final String addressFrom;
  final String addressTo;
  final String baggage;
  final String formattedDate;
  final String formattedTime;

  PassengerInfo(
      {
        this.id,
        this.name,
        this.surname,
        this.phone,
        this.addressFrom,
        this.addressTo,
        this.baggage,
        this.formattedDate,
        this.formattedTime
      }
    );

  PassengerInfo.fromJSON(json)
  : id = json["id"],
    name = json["name"],
    surname = json["surname"],
    phone = json["phone"],
    addressFrom = json["addressFrom"],
    addressTo = json["addressTo"],
    baggage = json["baggage"],
    formattedDate = json["formattedDate"],
    formattedTime = json["formattedTime"];


  String toJSON() {
    final passengerInfoObj = {
      "id": id,
      "name" : name,
      "surname" : surname,
      "phone" : phone,
      "addressFrom" : addressFrom,
      "addressTo" : addressTo,
      "baggage" : baggage,
      "formattedDate" : formattedDate,
      "formattedTime" : formattedTime,
    };
    return JsonEncoder().convert(passengerInfoObj);
  }

  @override
  List<Object> get props => [
    id,
    name,
    surname,
    phone,
    addressFrom,
    addressTo,
    baggage,
    formattedDate,
    formattedTime,
  ];
}