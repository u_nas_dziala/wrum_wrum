import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wrum_wrum/bloc/archived_routes/archived_routes_bloc.dart';
import 'package:wrum_wrum/bloc/current_archived_route/current_archived_route_bloc.dart';
import 'package:wrum_wrum/db/local_db_provider.dart';

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Historia"),
      ), body: HistoryContent(),
    );
  }
}

class HistoryContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ArchivedRoutesBloc, ArchivedRoutesState>(
        builder: (context, state) {
      if (state is ArchivedRoutesLoadInProgress) {
        return CircularProgressIndicator();
      } else if (state is ArchivedRoutesLoadSuccess) {
        final routes = state.routes;
        print("aaaaaaaaaaaaaaaa");
        print(routes);
        //todo: check for empty table == null
        return ListView.builder(
            itemCount: routes.length,
            itemBuilder: (BuildContext context, int index) {
              final route = routes[index];
              // TODO: Change it so that it is not hardcoded but calculated
              final distance = 500;
              return GestureDetector(
                onTap: () {
                  BlocProvider.of<CurrentArchivedRouteBloc>(context).add(CurrentArchivedRouteStarted(route));
                  DefaultTabController.of(context).animateTo(DefaultTabController.of(context).length - 1);
                },
                child: Container(
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: getFirstWord(route.addressFrom),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                TextSpan(
                                  text: "   ➝   ",
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                TextSpan(
                                    text: getFirstWord(route.addressTo),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 17,
                                        fontWeight: FontWeight.w600)),
                              ]),
                            )
                          ],
                        ),
                        Icon(
                          Icons.directions_car_outlined,
                          size: 40,
                        )
                      ],
                    ),
                  ),
                ),
              );
            });
      } else {
        return Container();
      }
    });
  }

  getFirstWord(String addressFrom) {
    var splitedTab = addressFrom.split(" ");
    String result = splitedTab.first;
    return result.split(",").first;
  }
}
