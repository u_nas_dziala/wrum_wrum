import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:wrum_wrum/bloc/current_archived_route/current_archived_route_bloc.dart';
import 'package:wrum_wrum/model/passanger_info.dart';
import 'package:wrum_wrum/model/route_tras.dart';

class ArchivedRoutes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CurrentArchivedRouteBloc, CurrentArchivedRouteState>(
        builder: (context, state) {
      if (state is CurrentArchivedRouteNotSelected) {
        return Container();
      } else if (state is CurrentArchivedRouteSelected) {
        final RouteTras route = state.route;
        return Column(children: [
          AboutRouteBox(route),
          Expanded(
            child: AboutPassengers(route.passengerList),
          ),
        ]);
      } else {
        return Container();
      }
    });
  }
}

class AboutRouteBox extends StatelessWidget {
  final RouteTras route;

  const AboutRouteBox(this.route, {Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    final from = getFirstWord(route.addressFrom);
    final to = getFirstWord(route.addressTo);
    const distance = 294;


    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.all(30),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 2),
            ),
          ]
      ),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                          text: from,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.w600,

                          ),
                        ),
                        TextSpan(
                          text: "   ➝   ",
                          style: TextStyle(
                            color: Colors.black87,
                            fontSize: 17,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        TextSpan(
                            text: to,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontWeight: FontWeight.w600
                            )
                        ),
                      ]
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: distance.toString(),
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                        ),
                      ),
                      TextSpan(
                          text: " km",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16
                          )
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: "02.12.2020",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13
                            ),
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            text: "20:20",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                      ),
                      child: RichText(
                        text: TextSpan(
                          text: "➝",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 13
                          ),
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: "02.12.2020",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13
                            ),
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            text: "20:20",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Material(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: InkWell(
                  customBorder: new CircleBorder(),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Icon(
                      Icons.alt_route_sharp,
                      size: 40,
                    ),
                  ),
                  onTap: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getFirstWord(String addressFrom) {
    var splitedTab = addressFrom.split(" ");
    String result = splitedTab.first;
    return result.split(",").first;
  }
}

class AboutPassengers extends StatelessWidget {
  final List<PassengerInfo> passengersNotHardcoded;

  const AboutPassengers(this.passengersNotHardcoded, {Key key}): super(key: key);

  Widget buildAboutPassenger(PassengerInfo passenger) {
    String name = passenger.name;
    String surname = passenger.surname;
    String phone = passenger.phone;
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
              color: Colors.blueAccent,
          )
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name+' '+surname,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  phone,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            RatingBar.builder(
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemSize: 30.0,
              itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    final List<PassengerInfo> passengers = [
      PassengerInfo(
          id: 0,
          name: "Maciej",
          surname: "Nowak",
          phone: "795 504 932",
          addressFrom: "ul. Kwiatowa 2 Kraków",
          addressTo: "ul. Piastów 3 Warszawa",
          baggage: "walizka",
          formattedDate: "7/1/2021 18:00",
          formattedTime: "7/1/2021 21:00"
      ),
      PassengerInfo(
          id: 1,
          name: "Jan",
          surname: "Kowalski",
          phone: "604 594 432",
          addressFrom: "ul. Budryka 4 Kraków",
          addressTo: "ul. Jagiellonów 5 Warszawa",
          baggage: "torba",
          formattedDate: "7/1/2021 18:30",
          formattedTime: "7/1/2021 21:00"
      ),
      PassengerInfo(
          id: 0,
          name: "Michał",
          surname: "Kowal",
          phone: "795 504 932",
          addressFrom: "ul. Kwiatowa 2 Kraków",
          addressTo: "ul. Piastów 3 Warszawa",
          baggage: "walizka",
          formattedDate: "7/1/2021 18:00",
          formattedTime: "7/1/2021 21:00"
      )];

    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: passengers.length,
        itemBuilder: (BuildContext context, int index) {
          return buildAboutPassenger(passengers[index]);
        }
    );
  }
}


