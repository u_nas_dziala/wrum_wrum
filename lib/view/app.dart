import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wrum_wrum/bloc/archived_routes/archived_routes_bloc.dart';
import 'package:wrum_wrum/bloc/current_archived_route/current_archived_route_bloc.dart';
import 'package:wrum_wrum/bloc/current_routes/current_routes_bloc.dart';
import 'package:wrum_wrum/db/local_db_provider.dart';
import 'package:wrum_wrum/repository/route_repository.dart';
import 'package:wrum_wrum/bloc/current_route/current_route_bloc.dart';
import 'package:wrum_wrum/view/current_route.dart';
import 'package:wrum_wrum/view/planning.dart';
import 'package:wrum_wrum/view/archived_routes.dart';

import 'history.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LocalDBProvider.db.updateIfPlannedIsHistorical();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => ArchivedRoutesBloc(RouteRepository())
                ..add(ArchivedRoutesLoaded())),
          BlocProvider(
              create: (context) => CurrentRoutesBloc(RouteRepository())
                ..add(CurrentRoutesLoaded())),
          BlocProvider(create: (context) => CurrentArchivedRouteBloc()),
          BlocProvider(create: (context) => CurrentRouteBloc()),
        ],
        child: MaterialApp(
          home: BlocBuilder<CurrentRouteBloc, CurrentRouteState>(
            builder: (context, state) {
              List<Tab> tabBarElems = [];
              List<Widget> tabViewElems = [];

              if (state is CurrentRouteSelected) {
                tabBarElems.add(Tab(icon: Icon(Icons.directions_car_rounded)));
                tabViewElems.add(CurrentRoute());
              }

              tabBarElems.addAll([
                Tab(icon: Icon(Icons.list)),
                Tab(icon: Icon(Icons.history_edu)),
                Tab(icon: Icon(Icons.people)),
              ]);

              tabViewElems.addAll([
                Planning(),
                History(),
                ArchivedRoutes(),
              ]);

              return DefaultTabController(
                  length: tabBarElems.length,
                  child: Scaffold(
                    bottomNavigationBar: Material(
                      color: Colors.blue,
                      child: TabBar(tabs: tabBarElems),
                    ),
                    body: TabBarView(children: tabViewElems),
                  ));
            },
          ),
        ));
  }
}
