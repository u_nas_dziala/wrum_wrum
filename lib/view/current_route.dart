import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wrum_wrum/bloc/current_route/current_route_bloc.dart';
import 'package:wrum_wrum/model/passanger_info.dart';
import 'package:wrum_wrum/model/route_tras.dart';
import 'package:wrum_wrum/view/add_passenger_view.dart';

class CurrentRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CurrentRouteBloc, CurrentRouteState>(
        builder: (context, state) {
          if (state is CurrentRouteNotSelected) {
            return Container();
          } else if (state is CurrentRouteSelected) {
            final RouteTras route = state.route;
            return Scaffold(
                appBar: AppBar(
                  title: Text("Aktualna trasa"),
                ),
                body: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    AboutRouteBox(route),
                    SizedBox(
                      height: 15,
                    ),
                    Flexible(
                      child: ListView(
                            children: [
                              PassengerAbout(route.passengerList),
                              RoutePlan()
                            ]
                      ),
                    ),
                  ],
                ),
                floatingActionButton: FloatingActionButton(
                  child: Icon(Icons.person),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => AddPassengerView(route.id)),
                    );
                  },
                )
            );
          } else {
            return Container();
          }
        });
  }
}

class AboutRouteBox extends StatelessWidget {
  final RouteTras route;

  const AboutRouteBox(this.route, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //const from = "Kraków";
    final from = getFirstWord(route.addressFrom);
    final to = getFirstWord(route.addressTo);
    const distance = 294;

    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 2),
            ),
          ]),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(children: [
                    TextSpan(
                      text: from,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: "   ➝   ",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                        text: to,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.w600)),
                  ]),
                ),
                SizedBox(
                  height: 3,
                ),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: distance.toString(),
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                      TextSpan(
                          text: " km",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16))
                    ],
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: "02.12.2020",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13),
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            text: "20:20",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                      ),
                      child: RichText(
                        text: TextSpan(
                          text: "➝",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 13),
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: "02.12.2020",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13),
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            text: "20:20",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Material(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: InkWell(
                  customBorder: new CircleBorder(),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Icon(
                      Icons.alt_route_sharp,
                      size: 40,
                    ),
                  ),
                  onTap: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getFirstWord(String addressFrom) {
    var splitedTab = addressFrom.split(" ");
    String result = splitedTab.first;
    return result.split(",").first;
  }
}

class PassengerAbout extends StatelessWidget {
  final List<PassengerInfo> passengersNotHardcoded;

  const PassengerAbout(this.passengersNotHardcoded, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<PassengerInfo> passengers = [
      PassengerInfo(
          id: 0,
          name: "Maciej",
          surname: "Placek",
          phone: "795 504 932",
          addressFrom: "ul. Kwiatowa 2 Kraków",
          addressTo: "ul. Piastów 3 Warszawa",
          baggage: "walizka",
          formattedDate: "7/1/2021 18:00",
          formattedTime: "7/1/2021 21:00"
      ),
      PassengerInfo(
          id: 1,
          name: "Jan",
          surname: "Kowalski",
          phone: "604 594 432",
          addressFrom: "ul. Budryka 4 Kraków",
          addressTo: "ul. Jagiellonów 5 Warszawa",
          baggage: "torba",
          formattedDate: "7/1/2021 18:30",
          formattedTime: "7/1/2021 21:00"
      ),
    ];
    final List<Widget> passengerEntries = passengersNotHardcoded.map(
        (passenger) => Container(
          child: Row(
                children: [
                  Text(passenger.name + " " + passenger.surname),
                  Spacer(),
                  Text("tel. "),
                  Text(passenger.phone)
                ],
              )
        ),
    ).toList();

    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: 0.0,
          horizontal: 30.0
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
              "Pasażerowie:",
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold
              )
          ),
          Column(
            children: passengerEntries,
          )
        ],
      ),
    );
  }
}

enum RouteCheckpointType {
  START,
  PICK_PERSON,
  FUEL,
  END
}

class RoutePlan extends StatelessWidget {
  Widget buildCheckpointWidget(String city, String street, RouteCheckpointType type, {String name}) {
    IconData currentIcon = null;
    String checkpointComment = "";

    switch(type) {
      case RouteCheckpointType.START:
        currentIcon = Icons.home;
        checkpointComment = "Początek jazdy";
        break;
      case RouteCheckpointType.PICK_PERSON:
        currentIcon = Icons.person;
        checkpointComment = "Odebrać: " + name;
        break;
      case RouteCheckpointType.END:
        currentIcon = Icons.flag;
        checkpointComment = "Zakończenie jazdy";
        break;
      case RouteCheckpointType.FUEL:
        currentIcon = Icons.bathtub;
        checkpointComment = "Zatankować";
        break;
    }

    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.all(4),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: Colors.blueAccent
          )
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  city,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  street,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  checkpointComment,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Icon(
                currentIcon,
                size: 40,
              ),
            ),
          ],
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildCheckpointWidget("Kraków", "Budryka 8", RouteCheckpointType.START),
        buildCheckpointWidget("Kraków", "Circle K", RouteCheckpointType.FUEL),
        buildCheckpointWidget("Zamość", "Wyszyńskiego 1", RouteCheckpointType.END),
      ],
    );
  }
}
