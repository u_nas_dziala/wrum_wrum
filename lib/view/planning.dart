import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wrum_wrum/bloc/current_route/current_route_bloc.dart';
import 'package:wrum_wrum/bloc/current_routes/current_routes_bloc.dart';
import 'package:wrum_wrum/db/local_db_provider.dart';

import 'add_route_view.dart';

class Planning extends StatelessWidget {
  /*final TabController

  const Planning*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Planowane"),
      )
        , body: PlanningContent(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddRouteView()),
          );
        },
      ),
    );
  }
}

class PlanningContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CurrentRoutesBloc, CurrentRoutesState>(
        builder: (context, state) {
          if (state is CurrentRoutesLoadInProgress) {
            return CircularProgressIndicator();
          } else if (state is CurrentRoutesLoadSuccess) {

            final routes = state.routes;
            return ListView.builder(
                itemCount: routes.length,
                itemBuilder: (BuildContext context, int index) {
                  final route = routes[index];
                  final from = getFirstWord(route.addressFrom);
                  final to = getFirstWord(route.addressTo);
                  final distance = 500;
                  return GestureDetector(
                    onTap: () {
                      BlocProvider.of<CurrentRouteBloc>(context).add(CurrentRouteStarted(route));
                      DefaultTabController.of(context).animateTo(0);
                    },
                    child: Container(
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 2),
                            ),
                          ]
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RichText(
                                  text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: from,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 17,
                                            fontWeight: FontWeight.w600,

                                          ),
                                        ),
                                        TextSpan(
                                          text: "   ➝   ",
                                          style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 17,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                        TextSpan(
                                            text: to,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 17,
                                                fontWeight: FontWeight.w600
                                            )
                                        ),
                                      ]
                                  ),
                                ),
                              ],
                            ),
                            Icon(
                              Icons.directions_car_outlined,
                              size: 40,
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                });
          } else {
            return Container();
          }
        });
  }

  getFirstWord(String addressFrom) {
    var splitedTab = addressFrom.split(" ");
    String result = splitedTab.first;
    return result.split(",").first;
  }

  }

