import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:wrum_wrum/bloc/current_route/current_route_bloc.dart';
import 'package:wrum_wrum/bloc/current_routes/current_routes_bloc.dart';
import 'package:wrum_wrum/db/local_db_provider.dart';
import 'package:wrum_wrum/model/passanger_info.dart';

class AddPassengerView extends StatefulWidget {
  int routeID;
  AddPassengerView(this.routeID);

  @override
  State<StatefulWidget> createState() => AddPassengerViewState();
}

class AddPassengerViewState extends State<AddPassengerView> {

  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _surnameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _luggageController = TextEditingController();

  String formattedFromAddress = "";
  String formattedToAddress = "";

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();
  String formattedDateTime = "";

  updateFormattedDateTime() {
    formattedDateTime = DateFormat.yMd().format(selectedDate) + " [" + selectedTime.hour.toString() + ":" + selectedTime.minute.toString() + "]";
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      // TODO: Think if changing firstDate to DateTime.now() has any cons
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101));
    if(picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        updateFormattedDateTime();
      });
    }
  }
  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        updateFormattedDateTime();
      });
  }

  // TODO: Reformat this
  openFrom() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PlacePicker(
              apiKey: "AIzaSyDO_hTSaomv_jUJPW8c0BkXMZbi1ed2cO4",
              onPlacePicked: (result) {
                setState(() {
                  formattedFromAddress = result.formattedAddress;
                });
                Navigator.of(context).pop();
              },
              initialPosition: LatLng(30, 60),
              useCurrentLocation: true,
            ))
    );
  }
  openTo() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PlacePicker(
              apiKey: "AIzaSyDO_hTSaomv_jUJPW8c0BkXMZbi1ed2cO4",
              onPlacePicked: (result) {
                setState(() {
                  formattedToAddress = result.formattedAddress;
                });
                Navigator.of(context).pop();
              },
              initialPosition: LatLng(30, 60),
              useCurrentLocation: true,
            ))
    );
  }

  @override
  Widget build(BuildContext context) {
    updateFormattedDateTime();
    return Scaffold(
      appBar: AppBar(
        title: Text('Nowy pasażer'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [

              TextFormField(
                decoration: const InputDecoration(labelText: 'Imię'),
                controller: _nameController,
                validator: (value) {
                  if(value.length < 1) {
                    return 'Pole imię nie może być puste';
                  }
                  return null;
                },
              ),

              TextFormField(
                decoration: const InputDecoration(labelText: 'Nazwisko'),
                controller: _surnameController,
                validator: (value) {
                  if(value.length < 1) {
                    return 'Pole nazwisko nie może być puste';
                  }
                  return null;
                },
              ),

              TextFormField(
                decoration: const InputDecoration(labelText: 'Telefon'),
                controller: _phoneController,
                keyboardType: TextInputType.number,
                validator: (value) {
                  if(value.length < 1) {
                    return 'To pole nie może być puste';
                  }
                  return null;
                },
              ),

              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: RaisedButton(
                      onPressed: () => openFrom(),
                      child: Text('Skąd'),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 1,
                    child: RaisedButton(
                      onPressed: () => openTo(),
                      child: Text('Dokąd'),
                    ),
                  )
                ],
              ),

              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(formattedFromAddress),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(formattedToAddress),
                  ),
                ],
              ),

              TextFormField(
                decoration: const InputDecoration(labelText: 'Bagaż'),
                controller: _luggageController,
                validator: (value) {
                  if(value.length < 1) {
                    return 'Pole imię nie może być puste';
                  }
                  return null;
                },
              ),

              Row(
                children: [
                  Expanded(
                      flex: 4,
                      child: Text(formattedDateTime)
                  ),
                  Expanded(
                    flex: 2,
                    child: RaisedButton(
                      onPressed: () => _selectDate(context),
                      child: Text('Zmień dzień'),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    flex: 2,
                    child: RaisedButton(
                      onPressed: () => _selectTime(context),
                      child: Text('Zmień godzinę'),
                    ),
                  ),
                ],
              ),
              Spacer(),

              ElevatedButton(
                onPressed: () {
                  addToDatabase();
                  Navigator.pop(context);
                  },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                  child: Text(
                    "Dodaj",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),

            ]
          )
        )
      )
    );
  }

  addToDatabase() {
    String name = _nameController.text.toString();
    String surname = _surnameController.text.toString();
    String phone = _phoneController.text.toString();
    String addressFrom = formattedFromAddress;
    String addressTo = formattedToAddress;
    String baggage = _luggageController.text.toString();
    String formattedDate = selectedDate.day.toString() + "/" + selectedDate.month.toString() + "/" + selectedDate.year.toString();
    String formattedTime = selectedTime.hour.toString() + ":" + selectedTime.minute.toString();

    var newDBPassenger = PassengerInfo(
      name:name,
      surname:surname,
      phone:phone,
      addressFrom:addressFrom,
      addressTo:addressTo,
      baggage:baggage,
      formattedDate:formattedDate,
      formattedTime:formattedTime,
    );

    BlocProvider.of<CurrentRouteBloc>(context).add(CurrentRoutePassengerAdded(newDBPassenger, widget.routeID));
  }
}
